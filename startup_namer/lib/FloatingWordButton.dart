import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FloatingWordButton extends StatelessWidget {
  FloatingWordButton({@required this.onPressed});
  final GestureTapCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: Colors.redAccent,
      splashColor: Colors.pinkAccent.shade100,
      child: Text(
        "Use word",
        style: TextStyle(color: Colors.white),
      ),
      onPressed: onPressed,
      shape: const StadiumBorder(),
    );
  }
}
